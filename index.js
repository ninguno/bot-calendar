// Bot-calendar v0.1 by ninguno & m

'use strict'

const xmpp = require('node-xmpp-client'),
    fs = require('fs'),
    user = 'erb0t@suchat.org',
    pass = '',
    host = 'suchat.org',
    port = 5222,
    room = 'hacklab',
    muc = 'grupo.ingobernable.net',
    nick = 'Calendar',
    filename = 'calendar.json',
    regexp = new RegExp('^evento [0-3]{1}?[0-9]-[0-1]{1}?[0-9]-20[0-9]{2} [0-2]{1}?[0-9]{1}:[0-5]{1}?[0-9]{1} .*'),
    client = new xmpp({ jid: user, password: pass, host: host, port: port, reconnect: true }),
    avisos = [
        { mtime: 0, aviso: 'ha comenzado, disfruta :-)' },
        { mtime: 60000, aviso: 'comenzará en un minuto...' },
        { mtime: 300000, aviso: 'comenzará en 5 minutos...' },
        { mtime: 900000, aviso: 'comenzará en un cuarto de hora...' },
        { mtime: 1800000, aviso: 'comenzará en media hora...' },
        { mtime: 3600000, aviso: 'comenzará en una hora...' }
    ]

if (!fs.exists(__dirname + '/' + filename)) {
    let object = {}
    object.index = 'eventos'
    object.eventos = []
    fs.writeFileSync(__dirname + '/' + filename, JSON.stringify(object))
}

const file = JSON.parse(fs.readFileSync(__dirname + '/' + filename))

client.connection.socket.on('error', error => {
    console.error(error)
    process.exit(1)
})

client.on('online', data => {
    console.log('Connected as ' + data.jid.local + '@' + data.jid.domain + '/' + data.jid.resource)
    client.send(new xmpp.Stanza('presence', { from: data.jid.local + '@' + data.jid.domain, to: room + '@' + muc + '/' + nick })
        .c('x', { xmlns: 'http://jabber.org/protocol/muc' })
        .c('show').t('groupchat').up()
        .c('status').t('Bot-calendar')
    )
    client.send(new xmpp.Stanza('message', { to: room + '@' + muc, type: 'groupchat' }))
    file.eventos.map(event => {
        let e = new Date(event.fecha)
        if (e.getTime() > Date.now())
            eventDate(event, room + '@' + muc + '/' + nick)
    })
})

client.on('stanza', stanza => {
    if (stanza.is('message') && stanza.attrs.type === 'groupchat' && stanza.getChildText('body')) {
        if (stanza.getChildText('body').match(/^evento .*/)) {
            let reply = new xmpp.Stanza('message', { from: stanza.attrs.to, to: room + '@' + muc, type: 'groupchat' })
            if (stanza.getChildText('body') && stanza.getChildText('body').match(regexp)) {
                let entry = stanza.getChildText('body').match(regexp),
                    year = parseInt(entry['input'].split('-')[2].split(' ')[0]),
                    month = parseInt(entry['input'].split('-')[1]) - 1,
                    day = parseInt(entry['input'].split('-')[0].split(' ')[1]),
                    hours = parseInt(entry['input'].split(':')[0].split(' ')[2]),
                    minutes = parseInt(entry['input'].split(':')[1].split(' ')[0]),
                    evento = entry['input'].split(' ').slice(3).join(' '),
                    fecha = new Date(year, month, day, hours, minutes, 0, 0)
                if (fecha.getTime() > Date.now()
                    && !file.eventos.some(event => new Date(event.fecha).getTime() === fecha.getTime())
                    && !file.eventos.some(event => event.evento.trim() === evento.trim())) {
                    let event = { evento: evento, fecha: fecha }
                    file.eventos.push(event)
                    fs.writeFileSync(__dirname + '/' + filename, JSON.stringify(file))
                    eventDate(event, stanza.attrs.to)
                    reply.c('body').t('Evento: ' + evento + ' añadido el ' + fecha.getDate() + '/' + (parseInt(fecha.getMonth()) + 1) + '/' + parseInt(fecha.getFullYear()) + ' a las ' + fecha.getHours() + ':' + (fecha.getMinutes() == 0 ? '0' + fecha.getMinutes() : fecha.getMinutes()) + ', no olvides asistir :-)')
                } else reply.c('body').t('Lo sentimos la fecha debe ser posterior a esa fecha o el evento ya existe, gracias')
            } else reply.c('body').t('Para añadir un evento complete fecha y hora: evento ##-##-#### ##:## texto del evento')
            client.send(reply)
        }
        else if (stanza.getChildText('body') === 'eventos') {
            file.eventos.map((event, index) => {
                let e = new Date(event.fecha)
                if (e.getTime() > Date.now()) {
                    let reply = new xmpp.Stanza('message', { from: stanza.attrs.to, to: room + '@' + muc, type: 'groupchat' })
                    reply.c('body').t('Evento ' + (index + 1) + ': ' + event.evento + ' el día ' + e.getDate() + '/' + (parseInt(e.getMonth()) + 1) + '/' + parseInt(e.getFullYear()) + ' a las ' + e.getHours() + ':' + (e.getMinutes() == 0 ? '0' + e.getMinutes() : e.getMinutes()))
                    client.send(reply)
                }
            })
        }
    }
})

const eventDate = (event, stanza) => {
    let e = new Date(event.fecha)
    if (e.getTime() > Date.now()) {
        avisos.map(aviso => {
            if(((e.getTime() - Date.now()) - aviso.mtime) > 0) {
                setTimeout(() => {
                    let reply = new xmpp.Stanza('message', { from: stanza, to: room + '@' + muc, type: 'groupchat' })
                    reply.c('body').t('El evento: ' + event.evento + ' ' + aviso.aviso)
                    client.send(reply)
                }, (e.getTime() - Date.now()) - aviso.mtime)
            }
        })
    }
}

